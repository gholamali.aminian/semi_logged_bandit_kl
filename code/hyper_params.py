import yaml
from easydict import EasyDict as edict
import os


def load_config(path):
    with open(path, "r", encoding="utf8") as f:
        return edict(yaml.safe_load(f))


def load_hyper_params(config_path, proportion=None):
    hyper_params = load_config(config_path)
    if proportion is not None:
        hyper_params["train_limit"] = int(50_000 * proportion)
        hyper_params["experiment"]["name"] += "_p_" + str(proportion)
    common_path = hyper_params["dataset"]
    common_path += f"_{hyper_params.experiment.name}_"
    common_path += "_wd_" + str(hyper_params["weight_decay"])
    common_path += "_lamda_" + str(hyper_params["lamda"])
    common_path += "_train_limit_" + str(hyper_params["train_limit"])
    hyper_params["tensorboard_path"] = "tensorboard_stuff/" + common_path
    hyper_params["log_file"] = "saved_logs/" + common_path
    hyper_params["summary_file"] = "accs/" + common_path
    hyper_params["output_path"] = "models/outputs/" + common_path
    os.makedirs(os.path.dirname(hyper_params["tensorboard_path"]), exist_ok=True)
    os.makedirs(os.path.dirname(hyper_params["log_file"]), exist_ok=True)
    os.makedirs(os.path.dirname(hyper_params["summary_file"]), exist_ok=True)
    os.makedirs(os.path.dirname(hyper_params["output_path"]), exist_ok=True)
    return hyper_params
